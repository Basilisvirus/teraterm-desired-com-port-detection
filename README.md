# TeraTerm desired com port detection

Automatic detection of the desired com port with TeraTerm (but using response from the mcu upon connection)


Use this, while in the directory of your script to run it:


``` cmd /c .\ttpmacro.exe /V /I .\microdiagnosis.ttl ```
 
 
(/V makes the Macro window invisible)

If you want to make the TeraTerm window, inside your script (microdiagnosis.ttl) you will need to use the showtt -1 , after it connects to a device.

